SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
DIR = $(notdir $(shell pwd))
ifneq (,$(wildcard ./.env))
        include .env
        export
endif
IMAGE = docker.io/bitnami/postgresql:13

.PHONY: cmd-exists-%

cmd-exists-%:
    @hash $(*) > /dev/null 2>&1 || \
        (echo "ERROR: '$(*)' must be installed and available on your PATH."; exit 1)

client: cmd-exists-docker
    docker run -e PGPASSWORD -it --network $(DIR)_default --rm -v $(PWD)/data:/bitnami/postgresql/data $(IMAGE) psql -h $(DIR)_postgresql_1 -d postgres -U postgres -w
