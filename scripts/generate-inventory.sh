#!/usr/bin/env bash
set -euo pipefail

ansible-inventory -i inventory --list --export --yaml --output inventory.yaml "$@"
