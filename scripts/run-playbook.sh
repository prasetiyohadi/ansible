#!/usr/bin/env bash
set -euo pipefail

echo -n "$@" | grep -q -- "-v" && export ANSIBLE_STDOUT_CALLBACK=yaml

podman run \
    -e ANSIBLE_STDOUT_CALLBACK \
    -e "ANSIBLE_REMOTE_USER=$USER" \
    -it --rm \
    -v "$PWD/env/ssh:/root/.ssh:z" \
    -v "$PWD:/workdir:z" \
    ansible \
    ansible-playbook -i inventory.yaml "$@"
